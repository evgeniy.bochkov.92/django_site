from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from .forms import CourseForm
from .models import Course


class FormMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form
        return context


class CourseListView(FormMixin, ListView):
    model = Course
    form = CourseForm
    fields = ['name']


class CourseCreateView(CreateView):
    model = Course
    fields = ['name']
    success_url = reverse_lazy('courses-list')


class CourseUpdateView(UpdateView):
    model = Course
    form = CourseForm
    fields = ['name']


class CourseDeleteView(DeleteView):
    model = Course
    success_url = reverse_lazy('courses-list')
