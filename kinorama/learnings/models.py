from django.db import models
from django.urls import reverse


class Course(models.Model):
    name = models.CharField("Название курса", max_length=30)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('course-update', kwargs={'pk': self.pk})


class Student(models.Model):
    first_name = models.CharField("Имя студента", max_length=30)
    second_name = models.CharField("Фамилия студента", max_length=30)
    last_name = models.CharField("Отчество студента", max_length=30)
    rating = models.DecimalField("Рейтинг студента", decimal_places=1, max_digits=5)
    courses = models.ManyToManyField(Course)

    def __str__(self):
        return f"{self.first_name} {self.second_name} {self.last_name}"


class Account(models.Model):
    login = models.CharField("Логин", max_length=30)
    password = models.CharField("Пароль", max_length=30)
    student = models.OneToOneField(Student, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return self.student.name


class Teacher(models.Model):
    first_name = models.CharField("Имя преподавателя", max_length=30)
    second_name = models.CharField("Фамилия преподавателя", max_length=30)
    last_name = models.CharField("Отчество преподавателя", max_length=30)
    salary = models.DecimalField("Зарплата преподавателя", decimal_places=2, max_digits=8)

    def __str__(self):
        return f"{self.first_name} {self.second_name} {self.last_name}"


class Topic(models.Model):
    name = models.CharField("Тема группы занятий", max_length=30)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Lesson(models.Model):
    name = models.CharField("Название занятия", max_length=30, default='')
    number = models.IntegerField("Номер занятия", null=True)
    teacher = models.OneToOneField(Teacher, on_delete=models.CASCADE, primary_key=True)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Schedule(models.Model):
    date = models.DateField("Дата занятия")
    time = models.TimeField("Время занятия")
    lesson = models.OneToOneField(Lesson, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return f"{self.date.strftime('%d-%m-%Y')} {self.time.strftime('%H:%M:%S')} " \
               f"Занятие #{str(self.lesson.number)} {self.lesson.name}"


class Homework(models.Model):
    topic = models.OneToOneField(Topic, on_delete=models.CASCADE)
    student = models.OneToOneField(Student, on_delete=models.CASCADE)
    score = models.IntegerField("Оценка за работу")

    def __str__(self):
        return f"{self.topic.name} | {self.student.__str__()} | {str(self.score)}"
