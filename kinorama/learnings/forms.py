from django import forms


class CourseForm(forms.Form):
    name = forms.CharField(label='Название курса', max_length=30)
